/*
ALGO:::-->>>

go left, right, up, down
check if destination
if yes, return with the path
if not, check 1) if not a barrier and 2) not outside\greater than the maze\mazeSize, and 3) not in the traversed list,
if 1) 2) and 3) hold true, append the current position to path
if any check is false, return
base condition: current position is the destination, so when From = To, return with Path

NEW ALGO:::-->>>

We have a path from (X1,Y1) to (X3,Y3), if we have an intermediate\next_move (X2,Y2) such that we have a path from (X2,Y2) to (X3,Y3)
(X2,Y2) could possibly be the move to the immediate left, right, up or down
(X2,Y2) should 1) not be a barrier 2) not be within the maze boundaries specified, and 3) not be in the traversed list
*/


solve(From, To, ActualPath) :- solve(From, To, [From], Path), reverse(Path,[],ActualPath).

solve([X,Y],[X,Y],Path,Path).

	
solve([X1,Y1],[X3,Y3],TraversedPath,Path) :- 
	mazeSize(X2_,_),
	X2 is X1 + 1, 
	X2 =< X2_,
	not(barrier(X2,Y1)),
	not(member([X2,Y1],TraversedPath)),
	solve([X2,Y1],[X3,Y3],[[X2,Y1]|TraversedPath],Path).

solve([X1,Y1],[X3,Y3],TraversedPath,Path) :- 
	X2 is X1 - 1, 
	X2 >= 1,
	not(barrier(X2,Y1)),
	not(member([X2,Y1],TraversedPath)),
	solve([X2,Y1],[X3,Y3],[[X2,Y1]|TraversedPath],Path).
	
solve([X1,Y1],[X3,Y3],TraversedPath,Path) :- 
	mazeSize(_,Y2_),
	Y2 is Y1 + 1, 
	Y2 =< Y2_,
	not(barrier(X1,Y2)),
	not(member([X1,Y2],TraversedPath)),
	solve([X1,Y2],[X3,Y3],[[X1,Y2]|TraversedPath],Path).
	
solve([X1,Y1],[X3,Y3],TraversedPath,Path) :- 
	Y2 is Y1 - 1, 
	Y2 >= 1,
	not(barrier(X1,Y2)),
	not(member([X1,Y2],TraversedPath)),
	solve([X1,Y2],[X3,Y3],[[X1,Y2]|TraversedPath],Path).

reverse([],A,A).
reverse([H|T],Acc,Result):-
	reverse(T,[H|Acc],Result).
	
	
printGrid([]).
printGrid(L) :- nl,
   divider,
   printBlock(L,L1),
   divider,
   printBlock(L1,L2),
   divider,
   printBlock(L2,_),
   divider.

printBlock(L,L3) :-
   printRow(L,L1), nl,
   blankLine,
   printRow(L1,L2), nl,
   blankLine,
   printRow(L2,L3), nl.

printRow(L,L3) :-
   write('+ '),
   printTriplet(L,L1), write(' | '),
   printTriplet(L1,L2), write(' | '),
   printTriplet(L2,L3),
   write(' +').


blankLine :-
   write('+         |         |         +'), nl.


% blankLine.

divider :-
   write('+---------+---------+---------+'), nl.

printTriplet(L,L3) :-
   printElement(L,L1), write('  '),
   printElement(L1,L2), write('  '),
   printElement(L2,L3).

printElement([X|L],L) :- var(X), !, write('.').
printElement([X|L],L) :- write(X).

/*
:- printGrid([_,_,_,_,_,_,_,_,_,
        _,_,_,_,_,_,_,_,_,
        _,_,_,_,_,_,_,_,_,
        _,_,_,_,_,_,_,_,_,
        _,_,_,_,_,_,_,_,_,
        _,_,_,_,_,_,_,_,_,
        _,_,_,_,_,_,_,_,_,
        _,_,_,_,_,_,_,_,_,
        _,_,_,_,_,_,_,_,_]).
*/
		