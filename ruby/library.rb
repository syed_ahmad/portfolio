class Calendar
  @@date = 0
  def initialize
    @@date = 0 if @@date == nil  # for the first time if date = nil, set it to 0, otherwise do nothing
  end

  def get_date
    @@date
  end

  def advance
    @@date = @@date + 1
  end
end


class Book
  def initialize(id, title, author)
    @id = id
    @title = title
    @author = author
    @due_date = nil # book is not checked out initially
  end

  def get_id
    @id
  end

  def get_title
    @title
  end

  def get_author
    @author
  end

  def get_due_date
    @due_date
  end

  def check_out(due_date)
    @due_date = due_date #sets the new due date to the one provided. The book will now be checked out
  end

  def check_in
    @due_date = nil # check in the book
  end

  def to_s
    "#{@id}: #{@title}, by #{@author}"
  end

  def to_s_customized
    "#{@id}: #{@title}, by #{@author}. Due Date: #{@due_date}"
  end
end

class Member
  attr_accessor :name, :has_library_card, :max_books_allowed
  def initialize(name, library)
    @name = name
    @library = library
    @books = []
    @max_books_allowed = 3
    @has_library_card = false
  end

  def get_name
    @name
  end

  def get_books
    @books
  end

  def check_out(book)
    @books << book if ((book.is_a?(Book) && (@max_books_allowed > @books.length)))
  end

  def return(book)
    @books.delete(book) if (book.is_a?(Book) && @books.include?(book)) # remove the book from the checked out books
  end

  def send_overdue_notice(notice)
    puts "#{@name} : #{notice}"
  end
end



class Library
  def initialize
    @id = 1
    @library_books = []
    @checked_out_books = []
    file = File.new('collection.txt', 'r') # open file in read mode
    while(line = file.gets)
      line = line.chomp
      data = line.split(',')
     (@library_books << Book.new(@id, data[0], data[1]); @id += 1) if (data != nil && data.length == 2)  # add into the library_books
    end

    @date = Calendar.new
    @members = Hash.new  # list of users currently using the library
                              # (in the form members[member_name][member_object])
                              # here default value is nil
    @is_open = false          # library is not open
    @current_member = nil     # currently serving nobody
  end



  def open
    @is_open ?  (puts 'The library is already open!') : (@date.advance;  @is_open = true; puts "Today is day #{@date.get_date}.")
  end

  def find_all_overdue_books
    @members.values.each { |member|
      puts "Overdue books for #{member.get_name}: "
      no_of_books_due = []
      member.get_books.each{ |book|
        (no_of_books_due << book ;puts book) if (book.get_due_date < @date.get_date) }
      no_of_books_due.length > 0 ? (;) : (puts 'None')
    }
  end

  def issue_card(name_of_member)
    if(!@is_open)
      puts 'The library is not open.'
    elsif(@members.include?(name_of_member))
      if(@members[name_of_member].has_library_card)
        puts "#{name_of_member} already has a library card."
      else
        @members[name_of_member].has_library_card = true;
        puts "Library card issued to #{name_of_member}."
      end
    else
      puts 'Sorry. Something went wrong!'
    end
  end

  def serve(name_of_member)
    if(!@is_open)
      puts 'The library is not open.'
    else
      if(@members.keys.include?(name_of_member))
        if(@members[name_of_member].has_library_card)
          @current_member = @members[name_of_member] # change currently serving member
          puts "Now serving #{name_of_member}."
        else
          puts "#{name_of_member} does not have a library card."
        end
      else
        puts "#{name_of_member} does not exist in the library."
      end
    end

  end

  def find_overdue_books
    # find overdue books for the member being served
    puts 'overdue books:'
    overdue_books = @current_member.get_books.select{ |book| book.get_due_date < @date.get_date }
    overdue_books.length > 0 ? (overdue_books.each { |book1| puts book1 }) : (puts 'None')
  end

  def check_in(*book_numbers)
    if(!@is_open)
      puts 'The library is not open.'
    elsif (@current_member == nil)
      puts 'No member is currently being served.'
    else
      # find book objects with the given book_numbers
      # call book.check_in for books
      # call member.return(book) for books
      # call library_books.add(book) for books
      # call check_out_books.remove(book) for books
      current_member_book_ids = []
      @current_member.get_books.each { |bk| current_member_book_ids << bk.get_id }
      books_not_found = book_numbers.select do |b_num| current_member_book_ids.index(b_num) == nil end

      if(books_not_found.length > 0)
        puts "The member does not have book(s) #{books_not_found.each { |book_not_found| book_not_found }}"
      else
        books_to_check_in = @current_member.get_books.select{ |book| book_numbers.index(book.get_id) != nil}
        books_to_check_in.each do |book| book.check_in
        @current_member.return(book)
        @checked_out_books.delete(book)
        @library_books << book  end
        puts "#{@current_member.get_name} has returned #{book_numbers.length} book(s)."
      end
    end
  end

  def search(string)
    if(string.length < 4)
      puts 'Search string must contain at least 4 characters.'
    else
    books_found = @library_books.select { |book|  (book.get_title.downcase.include?(string.downcase) ||
        book.get_author.downcase.include?(string.downcase))}
    puts 'search results:'
    books_found.each { |book| puts book }
    end
  end

  def check_out(*book_numbers)
    if(!@is_open)
      puts 'The library is not open.'
    elsif (@current_member == nil)
      puts 'No member is currently being served.'
    else
      # find book objects with the given book_numbers
      # call book.check_out for books
      # call member.check_out(book) for books
      # call library_books.remove(book) for books
      # call check_out_books.add(book) for books
      if((book_numbers.length + @current_member.get_books.length) > 3)
        puts 'A member can have at most 3 books checked out'
        puts "Cannot check_out #{book_numbers.length} more book(s). Member has #{@current_member.get_books.length} book(s)."
      else
        library_book_ids = []
        @library_books.each { |bk| library_book_ids << bk.get_id }
        books_not_found = book_numbers.select { |b_num| library_book_ids.index(b_num) == nil }

        if(books_not_found.length > 0)
          puts "The library does not have book(s) #{books_not_found.each { |book_not_found| book_not_found }}"
        else
          books_to_check_out = @library_books.select{ |book| book_numbers.index(book.get_id) != nil}
          books_to_check_out.each do |book| book.check_out( @date.get_date + 7)
          @current_member.check_out(book)
          @checked_out_books << book
          @library_books.delete(book)    end
          puts "#{book_numbers.length} book(s) have been checked out to #{@current_member.get_name}."
        end
      end
    end
  end


  def renew(*book_numbers)
    if(!@is_open)
      puts 'The library is not open.'
    elsif (@current_member == nil)
      puts 'No member is currently being served.'
    else
      current_member_book_ids = []
      @current_member.get_books.each { |bk| current_member_book_ids << bk.get_id }
      books_not_found = book_numbers.select do |b_num| current_member_book_ids.index(b_num) == nil end

      if(books_not_found.length > 0)
        puts "The member does not have book(s) #{books_not_found.each { |book_not_found| puts book_not_found }}"
      else
        books_to_renew = @current_member.get_books.select{|book| book_numbers.include?(book.get_id)}
        @checked_out_books.each{|book| book.check_out(@date.get_date + 7) if books_to_renew.include?(book)}
        books_to_renew.each {|book| book.check_out(@date.get_date + 7)}
        puts "#{book_numbers.length} book(s) have been renewed for #{@current_member.get_name}."
      end
    end
  end

  def close
   @is_open ? (@is_open = false; puts 'Good night.') : (puts 'The library is not open.')
  end

  def quit
    @is_open ? (puts 'First close the library') : (puts 'The library is now closed for renovations.')
  end

  def add_member(member_name)
    @members.keys.include?(member_name) ? (puts 'Member already exists in library' )
                                        :  @members[member_name] = Member.new(member_name,self)
  end

  def print_all_members
    puts 'library members:'
    @members.keys.each{|key| puts key}
  end

  def print_checked_out_books
    puts 'checked out books:'
    @checked_out_books.each { |book| puts book.to_s_customized}
  end

  def print_available_books
    puts 'available books:'
    @library_books.each { |book| puts book.to_s_customized}
  end

  def print_member_books
    puts 'member books:'
    @current_member.get_books.each { |book| puts book.to_s_customized }
  end

  def print_state
    print_checked_out_books
    print_available_books
    print_member_books
  end

end



library = Library.new

library.open
library.search("author")
usman = library.add_member("usman")
ahmad = library.add_member("ahmad")

member = Member.new("usman1",library)
library.issue_card("usman")
library.serve("usman")


#library.print_checked_out_books
#library.print_available_books


# searching books
library.search("title")

# checking out a book
library.check_out(1,2)

library.print_checked_out_books
library.print_available_books
library.print_member_books
#library.print_all_members


library.close
library.open
library.close
library.open
library.close
library.open
library.close
library.open
library.close
library.open
library.close
library.open
library.close
library.open

library.print_state

library.find_overdue_books

# find overdue books
#library.find_overdue_books

library.close
library.open

library.find_all_overdue_books

# serve ahmad
library.issue_card("ahmad")
library.serve("ahmad")

# search
library.search("title")

# valid check_out
library.check_out(5,6,7)

# invalid check_out
library.check_out(11)

library.print_state

library.find_all_overdue_books

# check invalid check_in
library.check_in(1)

# serve usman
library.serve("usman")
#library.check_in(1)

library.find_all_overdue_books
# renew book
#library.renew(1,2)
library.check_in(1,2)

library.print_state

library.check_out(1,3)
library.check_out(2)

library.check_in(1,2,3)

library.print_state

library.quit

library.close
library.quit