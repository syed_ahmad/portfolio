-module(converter).
-compile(export_all).
	
convert_temperature() ->
  receive
    {convert_to_celsius, TempF} ->
		TempC = (TempF - 32) * (5/9),
		spawn(?MODULE,display,[]) ! {convert_to_celsius, TempF, TempC},
		convert_temperature();
	{convert_to_fahrenheit, TempC} ->
		TempF = TempC * (9/5) + 32,
		spawn(?MODULE,display,[]) ! {convert_to_fahrenheit, TempF, TempC},
		convert_temperature();
	_ -> "Unexpected message received",
	    convert_temperature()
  end.

	
display() ->
  receive
    {convert_to_celsius,TempF, TempC} ->
		io:format("Temperature in Fahrenheit provided is: ~p~n", [TempF]),
		io:format("Temperature in Celsius would be: ~p~n", [TempC]),
		display();
	{convert_to_fahrenheit,TempF, TempC} ->
		io:format("Temperature in Celsius provided is ~p~n", [TempC]),
		io:format("Temperature in Fahrenheit would be: ~p~n", [TempF]),
		display();
	_ -> "Unexpected message received",
		display()
  end.

controller() ->
	receive
		test ->
		  Converter1 = spawn(?MODULE, convert_temperature, []),
		  timer:sleep(100),
		  Converter1 ! {convert_to_fahrenheit, 100},
		  timer:sleep(100),
		  Converter1 ! {convert_to_fahrenheit, 200},
		  timer:sleep(100),
		  Converter1 ! {convert_to_celsius, 10},
		  controller();
		_ ->
		  io:format("What? ~n"),
		  controller()
	end.